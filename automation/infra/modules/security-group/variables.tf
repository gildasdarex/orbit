variable "vpc_id" {
  description = "The ID of the VPC into which to deploy the cluster."
  type = string
}


variable "name" {
  description = "The name of the cluster to create."
  type = string
  default = "default"
}


variable "vpc_cidr_block" {
  type        = list
  default     = ["172.27.0.0/16"]
  description = "VPC CIDR block. See https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Subnets.html for more details"
}


variable "include_default_ingress_rule" {
  description = "Whether or not to include the default ingress rule on the ECS container instances security group (\"yes\" or \"no\")."
  type = string
  default = "yes"
}
variable "include_default_egress_rule" {
  description = "Whether or not to include the default egress rule on the ECS container instances security group (\"yes\" or \"no\")."
  type = string
  default = "yes"
}

variable "tags" {
  type        = "map"
  default     = {
    "platform" = "mybu"
  }
  description = "Additional tags (e.g. `map('BusinessUnit`,`XYZ`)"
}

variable "egress_cidrs" {
  description = "The CIDRs accessible from containers."
  type = list
  default = ["0.0.0.0/0"]
}