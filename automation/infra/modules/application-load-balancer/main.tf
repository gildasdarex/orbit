terraform {
  required_version = ">= 0.11"
}

resource "aws_security_group_rule" "instance_in_alb" {
  type                               = "ingress"
  from_port                          = 32768
  to_port                            = 61000
  protocol                           = "tcp"
  source_security_group_id           = "${module.alb_sg_https.this_security_group_id}"
  security_group_id                  = "${var.service_sg_id}"
}

module "alb_sg_https" {
  source                            = "git::https://github.com/terraform-aws-modules/terraform-aws-security-group.git"
  name                              = "${var.name}-alb-https"
  vpc_id                            = "${var.vpc_id}"
  ingress_with_cidr_blocks          = [
    {
      rule        = "all-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  egress_with_cidr_blocks           = [
    {
      rule        = "all-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  tags                              = "${var.tags}"
}


module "alb" {
  source  = "git::https://github.com/terraform-aws-modules/terraform-aws-alb.git"

  name = "${var.name}-alb"

  load_balancer_type = "application"

  vpc_id             = "${var.vpc_id}"
  subnets            = var.vpc_subnets
  security_groups    = ["${module.alb_sg_https.this_security_group_id}"]

  target_groups = [
    {
      name_prefix      = "pref-"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "instance"
    }
  ]


  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
    }
  ]

  tags = "${var.tags}"
}
