variable "region" {
  description = "The region into which to deploy the cluster."
  type = string
}

variable "tags" {
  type        = "map"
  default     = {
    "platform" = "mybu"
  }
  description = "Additional tags (e.g. `map('BusinessUnit`,`XYZ`)"
}

variable "availability_zones" {
  type        = "list"
  default     = ["us-east-1a", "us-east-1b"]
  description = "Availability Zones for the cluster"
}

variable "subnet_ids" {
  type        = "list"
  default     = []
  description = "Subnet to use for deployment"
}

variable "namespace" {
  type        = "string"
  default     = "livestorm"
  description = "Namespace, which could be your organization name, e.g. 'eg' or 'cp'"
}

variable "stage" {
  type        = "string"
  default     = "prod"
  description = "Stage, e.g. 'prod', 'staging', 'dev' or 'testing'"
}

variable "db_instance_type" {
  type        = "string"
  default     = "db.r4.large"
  description = ""
}


variable "attributes" {
  type        = "list"
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "vpc_cidr_block" {
  type        = "string"
  default     = "172.27.0.0/16"
  description = "VPC CIDR block. See https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Subnets.html for more details"
}
