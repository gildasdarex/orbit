output "aurora_db_id" {
  value = module.aurora_db_postgres96.cluster_identifier
}

output "cluster_endpoint" {
  value = join("", module.aurora_db_postgres96.cluster_endpoint)
}

// List of all DB instance endpoints running in cluster
output "all_instance_endpoints_list" {
  value = module.aurora_db_postgres96.all_instance_endpoints_list
}

// A read-only endpoint for the Aurora cluster, automatically load-balanced across replicas
output "reader_endpoint" {
value = join("", module.aurora_db_postgres96.reader_endpoint)
}