variable "tags" {
  type        = "map"
  default     = {
    "platform" = "mybu"
  }
  description = "Additional tags (e.g. `map('BusinessUnit`,`XYZ`)"
}


variable "name" {
  type        = "string"
  default     = "tdarex"
  description = "Solution name, e.g. 'app' or 'cluster'"
}

variable "cluster_instance_iam_policy_contents" {
  description = "The contents of the cluster instance IAM policy."
  type = string
  default = ""
}

variable "cluster_service_iam_policy_contents" {
  description = "The contents of the cluster service IAM policy."
  type = string
  default = ""
}
