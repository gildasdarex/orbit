variable "region" {
  description = "The region into which to deploy the cluster."
  type = string
}

variable "tags" {
  type        = "map"
  default     = {
    "platform" = "mybu"
  }
  description = "Additional tags (e.g. `map('BusinessUnit`,`XYZ`)"
}

variable "availability_zones" {
  type        = "list"
  default     = ["us-east-1a", "us-east-1b"]
  description = "Availability Zones for the cluster"
}

variable "namespace" {
  type        = "string"
  default     = "livestorm"
  description = "Namespace, which could be your organization name, e.g. 'eg' or 'cp'"
}

variable "stage" {
  type        = "string"
  default     = "prod"
  description = "Stage, e.g. 'prod', 'staging', 'dev' or 'testing'"
}

variable "name" {
  type        = "string"
  default     = "tdarex"
  description = "Solution name, e.g. 'app' or 'cluster'"
}


variable "attributes" {
  type        = "list"
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "vpc_cidr_block" {
  type        = "string"
  default     = "172.27.0.0/16"
  description = "VPC CIDR block. See https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Subnets.html for more details"
}

variable "cluster_instance_amis" {
  description = "A map of regions to AMIs for the container instances."
  type = map

  default = {
    us-east-1 = ""
    us-east-2 = ""
    us-west-1 = ""
    us-west-2 = ""
    ap-south-1 = ""
    ap-northeast-1 = ""
    ap-northeast-2 = ""
    ap-southeast-1 = ""
    ap-southeast-2 = ""
    ca-central-1 = ""
    cn-north-1 = ""
    eu-central-1 = ""
    eu-west-1 = ""
    eu-west-2 = ""
    eu-west-3 = ""
    sa-east-1 = ""
  }
}

variable "cluster_instance_user_data_template" {
  description = "The contents of a template for container instance user data."
  type = string
  default = ""
}

variable "cluster_instance_type" {
  description = "The instance type of the container instances."
  type = string
  default = "t2.medium"
}


variable "ssh_key_name" {
  type        = "string"
  default     = "ecs-nodes-key"
  description = "The ssh key that can be used to login to the cluster nodes"
}

variable "security_groups" {
  description = "The list of security group IDs to associate with the cluster."
  type = list
  default = []
}

variable "associate_public_ip_addresses" {
  description = "Whether or not to associate public IP addresses with ECS container instances (\"yes\" or \"no\")."
  type = string
  default = "no"
}


variable "cluster_minimum_size" {
  description = "The minimum size of the ECS cluster."
  type = string
  default = 1
}
variable "cluster_maximum_size" {
  description = "The maximum size of the ECS cluster."
  type = string
  default = 10
}
variable "cluster_desired_capacity" {
  description = "The desired capacity of the ECS cluster."
  type = string
  default = 3
}

variable "cluster_instance_root_block_device_size" {
  description = "The size in GB of the root block device on cluster instances."
  default = 30
}
variable "cluster_instance_root_block_device_type" {
  description = "The type of the root block device on cluster instances ('standard', 'gp2', or 'io1')."
  type = string
  default = "standard"
}

variable "egress_cidrs" {
  description = "The CIDRs accessible from containers."
  type = list
  default = ["0.0.0.0/0"]
}


variable "vpc_id" {
  description = "The CIDRs accessible from containers."
  type = string
  default = ""
}
variable "subnet_ids" {
  description = "The CIDRs accessible from containers."
  type = list
  default = []
}

