data "aws_ami" "amazon_linux_2" {
  most_recent = true
  owners = [
    "amazon"]

  filter {
    name = "name"
    values = [
      "amzn2-ami-ecs-hvm-*-x86_64-ebs"]
  }
}

data "template_file" "ami_id" {
  template = coalesce(lookup(var.cluster_instance_amis, var.region), data.aws_ami.amazon_linux_2.image_id)
}

module "security" {
  source              = "../security-group"
  tags                = var.tags
  name                = var.name
  vpc_id              = "${var.vpc_id}"
  vpc_cidr_block      = ["${var.vpc_cidr_block}"]
}

data "template_file" "cluster_user_data" {
  template = coalesce(var.cluster_instance_user_data_template, file("${path.module}/jenkins.tpl"))
  vars = {
    cluster_name = var.name
  }
}

resource "aws_launch_configuration" "cluster" {
  name_prefix = "jenkins-${var.name}"
  image_id = data.template_file.ami_id.rendered
  instance_type = var.cluster_instance_type
  key_name = var.ssh_key_name
  iam_instance_profile = module.iam.aws_iam_instance_profile
  user_data = data.template_file.cluster_user_data.rendered
  security_groups = concat(list(module.security.security_group_id), var.security_groups)
  associate_public_ip_address = var.associate_public_ip_addresses == "yes" ? true : false


  root_block_device {
    volume_size = var.cluster_instance_root_block_device_size
    volume_type = var.cluster_instance_root_block_device_type
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "cluster" {
  name_prefix = "asg-${var.name}"
  vpc_zone_identifier = "${var.subnet_ids}"
  launch_configuration = aws_launch_configuration.cluster.name
  min_size = var.cluster_minimum_size
  max_size = var.cluster_maximum_size
  desired_capacity = var.cluster_desired_capacity
}