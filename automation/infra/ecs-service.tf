module "ecs_cluster" {
  source = "./modules/ecs-cluster"
  name = "${var.name}"
  region = "${var.region}"
  tags = "${var.tags}"
  availability_zones = var.availability_zones
  ssh_key_name = "${var.ssh_key_name}"
  cluster_instance_type = "${var.cluster_instance_type}"
}

module "alb" {
  source = "./modules/application-load-balancer"
  name = "${var.name}"
  hostname = "${var.hostname}"
  domain_name = "${var.domain}"
  service_port = "${var.service_port}"
  service_protocol = "${var.service_protocol}"
  service_sg_id = "${module.ecs_cluster.security_group_id}"
  certificate_arn = "${var.certificate_arn}"
  tags = "${var.tags}"
  vpc_id = "${module.ecs_cluster.vpc_id}"
  vpc_subnets = module.ecs_cluster.public_subnet_ids
}