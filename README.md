# Orbit Architecture Documentation

Work time = 3h

![orbit architecture ](images/orbit_architecture_without_interaction.png)

## AWS Components / Services
    - VPC
    - Subnet
    - Internet Gateway
    - NAT Gateway
    - Application Load Balancer (ALB)
    - ECS Cluster
    - AWS ECR
    - Auto Scaling Group
    - VPC Endpoint
    - Cloudwatch
    - AWS ElastiCache 
    - AWS Aurora Database

## Architecture Components
    - VPC
    - 2 publics subnets (in 2 differents Availability Zone)
    - 2 privates subnets (in 2 differents Availability Zone)
    - Internet Gateway
    - 2 NAT Gateway
    - Application Load Balancer (to support Http 1/2 and websocket)
    - ECS Cluster + AWS Auto Scaling Group
    - Jenkins Cluster with AWS Auto Scaling Group
    - VPC Endpoint
    - Cloudwatch
    - Redis Cluster (AWS ElastiCache)
    - AWS Aurora Database


## Architecture Description

#### 1- VPC / Subnet
  A tiered VPC with public and private subnets, spanning an AWS region.   
  For High availability (HA) we will create 2 subnets in 2 differents 
  availability zones . Nodes of ECS cluster and database will be deployed on those
  2 availability zones to handle the case when one zone will be down (High availability).
  
  ECS cluster - Database - Redis will be deployed on private subnets (for security
  purpose) then they will not be directly accessible outside our infrastructure or from
  internet
  
 
 
 
     
#### 2- NAT Gateway
     
   ![Connexion to outside service architecture ](images/connexion_workflow.png)
   
   A NAT gateway is a Network Address Translation (NAT) service. 
   We can use a NAT gateway so that instances in a private subnet (Jenkins and ECS node) can connect to 
   services outside your VPC but external services cannot initiate a connection with those instances.
   



#### 3- ECS Cluster (with Auto Scaling Group)
  A highly available ECS cluster deployed across two Availability Zones in an Auto 
  Scaling group and that are AWS SSM enabled.
  
  In ECS cluster we will deploy our 3 microservices as docker containers with
  appropriate number of replicas and resources (cpu and memory ) requirements.
  **Number of replicas and resources configuration for each microservices should be
  determine in integration environnement with stress test (with jmeter by example)**
  
  Aws Auto Scaling Group will help us :
  - to increase the number of node of the ECS cluster  in specific availability 
  zone to handle the increase of workload in this zone (increase of requests / 
  increase of resources ...etc)
  - to decrease the number of node of the ECS cluster  in specific availability 
    zone to handle the decrease of workload in this zone (decrease of requests / 
    decrease of resources ...etc)
 


  
#### 4- Jenkins Cluster
![Jenkins details architecture ](images/jenkins_details_architecture.png)

For CI/CD purpose we will use jenkins cluster.
The schema above show in details how jenkins cluster will be organised in the architecture.
Master and slave nodes of jenkins will be deployed through Autoscaling group in our VPC   
in 2 differents availability zones to handle the case when one zone will be down . When
developers will push their code , the scenario is :
 - Jenkins job will be triggered
 - Jenkins job will build new docker image for microservice and stored it on AWS ECR 
 - Jenkins job will deployed new micorservice in AWS ECS


     
#### 5- Application Load Balancer
  An Application Load Balancer (ALB) to the public subnets to handle inbound traffic.
  ALB path-based routes for each ECS service to route the inbound traffic to the correct microservice
  or jenkins service .
  Application Load Balancer supports websocket so it can be used with our microservice that
  used websocket.
 
 
  
#### 6- AWS Aurora Postgres Database / Redis Cluster with AWS ElastiCache/ VPC Endpoint
     
   ![orbit architecture ](images/aurora_architecture.png)
   As database we will use Aws Aurora Postgres with read replicas configuration . We will  
   configure one writer (We can have multi writer). 
   As you can see in aurora architecture , Aurora DB provides us 2 endpoints : Writer 
   endpoint and Reader Endpoint .
   Request to insert data into master DB (M) should be done through writer endpoint.
   master DB is deployed in one availability zone .
   Request to read data from Reader DB (R) should be done through reader endpoint. Aws will
   load balance request according to workload to appropriate Reader database . 
   For High availability Reader database will be deployed accross our 2 availabilty zones
   and for writer endpoint it will be deployed into 2 in this case , but Aws will promote one 
   reader to master writer in case of failover .
   
   
   Redis cluster will be deployed through AWS ElastiCache . It supports Read replicas and 
   Multi AZ with auto failover
   
   As our database and redis cluster will be deployed in private subnet so they can not be 
   accessible from outside and by using dns , we will use vpc endpoint .
   A VPC endpoint enables connections between a virtual private cloud (VPC) and supported 
   services, without requiring that you use an internet gateway, NAT device, 
   VPN connection, or AWS Direct Connect connection. Therefore, you control the specific 
   API endpoints, sites, and services that are reachable from your VPC.
   


     
#### 7- AWS Cloudwatch
   Centralized container logging with Amazon CloudWatch Logs



   
#### 8- AWS ECR
  Aws service to store our docker image   
     
    



## Deployment Workflow
![deployment_architecture_flow ](images/deployment_architecture_flow.png)




## Customer Request Workflow
![customer_architecture_request_flow ](images/customer_architecture_request_flow.png)


## Improvments
 - Configure AWS 53 (for dns and our domain name managment)
 - Configure AWS Amazon CloudFront to speed up the distribution of static files 
 - Add Bastion server in public subnet.
   A bastion host is a server used to manage access to an internal or private network from an external network - sometimes called a jump box or jump server. 
   Because bastion hosts often sit on the Internet, they typically run a minimum amount of services in order to reduce their attack surface. 
   They are also commonly used to proxy and log communications, such as SSH sessions.
 - Amazon GuardDuty : Amazon GuardDuty is a service offered by AWS that can act as your cloud IDS. GuardDuty uses machine learning algorithms to monitor log sources, such as AWS CloudTrail and Amazon VPC Flow Logs, for any activity that could indicate unauthorized activity in your account.
                      
  